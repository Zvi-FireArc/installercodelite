1. download and install MinGW (make sure to install basic setup and all c++ compile and debug options)
2. download and install codelite
3. in project->settings->general , change 'Makefile Generator; to 'MinGW'
4. in project->settings->compiler->include paths, add VS include paths (e.g. C:\Program Files (x86)\Windows Kits\10\Include\10.0.19041.0\ucrt\)
5. open cmd.exe and run gdb , if it is not work download missing DLLs
6. run codelite IDE as admin