#include "Key.h"
#include <windows.h>


Key::Key()
{
    //ctor
}

Key::~Key()
{
    //dtor
}


void Key::PC(WORD wrd)
{
    INPUT ip = { 0 };
 // Set up a generic keyboard event.
    ip.type = INPUT_KEYBOARD;
    ip.ki.wScan = 0; // hardware scan code for key
    ip.ki.time = 0;
    ip.ki.dwExtraInfo = 0;

    // Press the key
    ip.ki.wVk = wrd;
    ip.ki.dwFlags = 0; // 0 for key press
    SendInput(1, &ip, sizeof(INPUT));
    ip.ki.dwFlags = KEYEVENTF_KEYUP; // KEYEVENTF_KEYUP for key release
    SendInput(1, &ip, sizeof(INPUT));
}

void Key::CK(WORD wrd1, WORD wrd2)
{
    INPUT ip1 = { 0 };
    INPUT ip2 = { 0 };

    // Set up a generic keyboard event.
    ip1.type = INPUT_KEYBOARD;
    ip1.ki.wScan = 0; // hardware scan code for key
    ip1.ki.time = 0;
    ip1.ki.dwExtraInfo = 0;

    // Set up a generic keyboard event.
    ip2.type = INPUT_KEYBOARD;
    ip2.ki.wScan = 0; // hardware scan code for key
    ip2.ki.time = 0;
    ip2.ki.dwExtraInfo = 0;


    ip1.ki.wVk = wrd1;
    ip1.ki.dwFlags = 0; // 0 for key press
    SendInput(1, &ip1, sizeof(INPUT));

    // Press the key
    ip2.ki.wVk = wrd2;
    ip2.ki.dwFlags = 0; // 0 for key press
    SendInput(1, &ip2, sizeof(INPUT));
    ip2.ki.dwFlags = KEYEVENTF_KEYUP; // KEYEVENTF_KEYUP for key release
    SendInput(1, &ip2, sizeof(INPUT));

    ip1.ki.dwFlags = KEYEVENTF_KEYUP; // KEYEVENTF_KEYUP for key release
    SendInput(1, &ip1, sizeof(INPUT));
}
