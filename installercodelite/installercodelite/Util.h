#ifndef UTIL_H
#define UTIL_H

#include <string>
using namespace std;

class Util
{
    public:
        Util();
        virtual ~Util();

        void FF();

    protected:

    private:

        void RP();
        void OB();
        void GTS();
        bool CS();
        string GetClText();
        void SetS();
        void Cl();
};

#endif // UTIL_H
