#ifndef KEY_H
#define KEY_H


class Key
{
    public:
        Key();
        virtual ~Key();

        static void PC(unsigned short wrd);
        static void CK(unsigned short wrd1, unsigned short wrd2);

    protected:

    private:
};

#endif // KEY_H
