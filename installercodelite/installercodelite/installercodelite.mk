##
## Auto Generated makefile by CodeLite IDE
## any manual changes will be erased      
##
## Debug
ProjectName            :=installercodelite
ConfigurationName      :=Debug
WorkspacePath          :=C:/Users/YairUni/Documents/installercodelite/installercodelite
ProjectPath            :=C:/Users/YairUni/Documents/installercodelite/installercodelite/installercodelite
IntermediateDirectory  :=$(ConfigurationName)
OutDir                 := $(IntermediateDirectory)
CurrentFileName        :=
CurrentFilePath        :=
CurrentFileFullPath    :=
User                   :=YairUni
Date                   :=28/12/2020
CodeLitePath           :="C:/Program Files/CodeLite"
LinkerName             :=C:/MinGW/bin/g++.exe
SharedObjectLinkerName :=C:/MinGW/bin/g++.exe -shared -fPIC
ObjectSuffix           :=.o
DependSuffix           :=.o.d
PreprocessSuffix       :=.i
DebugSwitch            :=-g 
IncludeSwitch          :=-I
LibrarySwitch          :=-l
OutputSwitch           :=-o 
LibraryPathSwitch      :=-L
PreprocessorSwitch     :=-D
SourceSwitch           :=-c 
OutputFile             :=$(IntermediateDirectory)/$(ProjectName)
Preprocessors          :=
ObjectSwitch           :=-o 
ArchiveOutputSwitch    := 
PreprocessOnlySwitch   :=-E
ObjectsFileList        :="installercodelite.txt"
PCHCompileFlags        :=
MakeDirCommand         :=makedir
RcCmpOptions           := 
RcCompilerName         :=C:/MinGW/bin/windres.exe
LinkOptions            :=  
IncludePath            :=  $(IncludeSwitch). $(IncludeSwitch). 
IncludePCH             := 
RcIncludePath          := 
Libs                   := 
ArLibs                 :=  
LibPath                := $(LibraryPathSwitch). 

##
## Common variables
## AR, CXX, CC, AS, CXXFLAGS and CFLAGS can be overriden using an environment variables
##
AR       := C:/MinGW/bin/ar.exe rcu
CXX      := C:/MinGW/bin/g++.exe
CC       := C:/MinGW/bin/gcc.exe
CXXFLAGS :=  -g -O0 -std=c++17 -std=c99 $(Preprocessors)
CFLAGS   :=  -g -O0 -Wall $(Preprocessors)
ASFLAGS  := 
AS       := C:/MinGW/bin/as.exe


##
## User defined environment variables
##
CodeLiteDir:=C:\Program Files\CodeLite
Objects0=$(IntermediateDirectory)/main.cpp$(ObjectSuffix) $(IntermediateDirectory)/JSONValue.cpp$(ObjectSuffix) $(IntermediateDirectory)/JSON.cpp$(ObjectSuffix) $(IntermediateDirectory)/Util.cpp$(ObjectSuffix) $(IntermediateDirectory)/Key.cpp$(ObjectSuffix) 



Objects=$(Objects0) 

##
## Main Build Targets 
##
.PHONY: all clean PreBuild PrePreBuild PostBuild MakeIntermediateDirs
all: $(OutputFile)

$(OutputFile): $(IntermediateDirectory)/.d $(Objects) 
	@$(MakeDirCommand) $(@D)
	@echo "" > $(IntermediateDirectory)/.d
	@echo $(Objects0)  > $(ObjectsFileList)
	$(LinkerName) $(OutputSwitch)$(OutputFile) @$(ObjectsFileList) $(LibPath) $(Libs) $(LinkOptions)

MakeIntermediateDirs:
	@$(MakeDirCommand) "$(ConfigurationName)"


$(IntermediateDirectory)/.d:
	@$(MakeDirCommand) "$(ConfigurationName)"

PreBuild:


##
## Objects
##
$(IntermediateDirectory)/main.cpp$(ObjectSuffix): main.cpp
	@$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) -MG -MP -MT$(IntermediateDirectory)/main.cpp$(ObjectSuffix) -MF$(IntermediateDirectory)/main.cpp$(DependSuffix) -MM main.cpp
	$(CXX) $(IncludePCH) $(SourceSwitch) "C:/Users/YairUni/Documents/installercodelite/installercodelite/installercodelite/main.cpp" $(CXXFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/main.cpp$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/main.cpp$(PreprocessSuffix): main.cpp
	$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) $(IntermediateDirectory)/main.cpp$(PreprocessSuffix) main.cpp

$(IntermediateDirectory)/JSONValue.cpp$(ObjectSuffix): JSONValue.cpp
	@$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) -MG -MP -MT$(IntermediateDirectory)/JSONValue.cpp$(ObjectSuffix) -MF$(IntermediateDirectory)/JSONValue.cpp$(DependSuffix) -MM JSONValue.cpp
	$(CXX) $(IncludePCH) $(SourceSwitch) "C:/Users/YairUni/Documents/installercodelite/installercodelite/installercodelite/JSONValue.cpp" $(CXXFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/JSONValue.cpp$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/JSONValue.cpp$(PreprocessSuffix): JSONValue.cpp
	$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) $(IntermediateDirectory)/JSONValue.cpp$(PreprocessSuffix) JSONValue.cpp

$(IntermediateDirectory)/JSON.cpp$(ObjectSuffix): JSON.cpp
	@$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) -MG -MP -MT$(IntermediateDirectory)/JSON.cpp$(ObjectSuffix) -MF$(IntermediateDirectory)/JSON.cpp$(DependSuffix) -MM JSON.cpp
	$(CXX) $(IncludePCH) $(SourceSwitch) "C:/Users/YairUni/Documents/installercodelite/installercodelite/installercodelite/JSON.cpp" $(CXXFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/JSON.cpp$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/JSON.cpp$(PreprocessSuffix): JSON.cpp
	$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) $(IntermediateDirectory)/JSON.cpp$(PreprocessSuffix) JSON.cpp

$(IntermediateDirectory)/Util.cpp$(ObjectSuffix): Util.cpp
	@$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) -MG -MP -MT$(IntermediateDirectory)/Util.cpp$(ObjectSuffix) -MF$(IntermediateDirectory)/Util.cpp$(DependSuffix) -MM Util.cpp
	$(CXX) $(IncludePCH) $(SourceSwitch) "C:/Users/YairUni/Documents/installercodelite/installercodelite/installercodelite/Util.cpp" $(CXXFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/Util.cpp$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/Util.cpp$(PreprocessSuffix): Util.cpp
	$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) $(IntermediateDirectory)/Util.cpp$(PreprocessSuffix) Util.cpp

$(IntermediateDirectory)/Key.cpp$(ObjectSuffix): Key.cpp
	@$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) -MG -MP -MT$(IntermediateDirectory)/Key.cpp$(ObjectSuffix) -MF$(IntermediateDirectory)/Key.cpp$(DependSuffix) -MM Key.cpp
	$(CXX) $(IncludePCH) $(SourceSwitch) "C:/Users/YairUni/Documents/installercodelite/installercodelite/installercodelite/Key.cpp" $(CXXFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/Key.cpp$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/Key.cpp$(PreprocessSuffix): Key.cpp
	$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) $(IntermediateDirectory)/Key.cpp$(PreprocessSuffix) Key.cpp


-include $(IntermediateDirectory)/*$(DependSuffix)
##
## Clean
##
clean:
	$(RM) -r $(ConfigurationName)/


